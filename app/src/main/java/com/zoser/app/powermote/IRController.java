package com.zoser.app.powermote;

import android.content.Context;
import android.hardware.ConsumerIrManager;

import java.util.LinkedList;
import java.util.Queue;

public class IRController extends Thread
{

   private final ConsumerIrManager irManager;

   private final boolean hasIREmitter;

   private final Object messageLock = new Object();
   private final Queue<IRMessageRequest> messageQueue = new LinkedList<IRMessageRequest>();

   public IRController(Context context)
   {
      this.irManager = (ConsumerIrManager)context.getSystemService(Context.CONSUMER_IR_SERVICE);
      this.hasIREmitter = irManager != null && irManager.hasIrEmitter();
   }

   private void executeMessage(IRMessageRequest request)
   {
      if(hasIREmitter) {
         for(IRMessage irMessage: request.getMessages()) {
            irManager.transmit(irMessage.getFrequency(), irMessage.getMessage());
            WaitPerMessage();
         }
      }
   }

   public boolean hasIREmitter() {
      return hasIREmitter;
   }

   public void sendMessage(IRMessageRequest request)
   {
      synchronized(messageLock)
      {
         int MAX_QUEUED_COUNT = 2;
         if(messageQueue.size() < MAX_QUEUED_COUNT) {
            messageQueue.add(request);
            messageLock.notify();
         }
      }
   }


   public void startWork()
   {
      this.start();
   }

   public void stopWork()
   {
      sendMessage(null);
      try
      {
         join();
      }
      catch(InterruptedException e)
      {
      }
   }

   @Override
   public void run()
   {
      while(true)
      {

         IRMessageRequest message = null;
         synchronized(messageLock)
         {
            if(messageQueue.size() > 0)
            {
               message = messageQueue.poll();

               if(message == null)
               {
                  break;
               }
            }
            else
            {
               try
               {
                  messageLock.wait();
               }
               catch(InterruptedException e)
               {
               }
            }
         }

         if(message != null)
         {
            executeMessage(message);
         }

         WaitPerPulse();

      }
   }

   private void WaitPerPulse()
   {
      try
      {
         int MAX_WAIT_PER_PULSE_MS = 40;
         Thread.sleep(MAX_WAIT_PER_PULSE_MS);
      }
      catch(InterruptedException e)
      {
         e.printStackTrace();
      }
   }


   private void WaitPerMessage()
   {
      try
      {
         int MAX_WAIT_PER_MESSAGE_MS = 40;
         Thread.sleep(MAX_WAIT_PER_MESSAGE_MS);
      }
      catch(InterruptedException e)
      {
         e.printStackTrace();
      }
   }
}