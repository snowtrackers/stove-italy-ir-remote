package fr.snowtrackers.stoveitaly_irremote;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;

import com.google.android.material.snackbar.Snackbar;
import com.zoser.app.powermote.IRController;
import com.zoser.app.powermote.IRMessageRequest;
import com.zoser.app.powermote.IRNecFactory;

public class MainActivity extends AppCompatActivity {

    private IRController irController;

    private boolean isDisplayCredits;
    private boolean isDisplayLicence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        this.irController = new IRController(getApplicationContext());

        this.isDisplayCredits = false;
        this.isDisplayLicence = false;

        if( this.irController.hasIREmitter() ) {
            this.irController.startWork();
        }
        else {
            this.displaySnackForNoIrEmitter();
        }
        this.initRemoteButtons();
        this.initOtherButtons();
    }

    @Override
    protected void onDestroy()
    {
        this.irController.stopWork();
        super.onDestroy();
    }

    private void initRemoteButtons() {
        Vibrator vibrator = (Vibrator)this.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

        this.findViewById(R.id.main_button_on_off).setOnClickListener(
                new IRButtonListener(vibrator, this, IRButtonListener.Action.ON_OFF) );

        this.findViewById(R.id.main_button_plus).setOnClickListener(
                new IRButtonListener(vibrator, this, IRButtonListener.Action.PLUS) );

        this.findViewById(R.id.main_button_minus).setOnClickListener(
                new IRButtonListener(vibrator, this, IRButtonListener.Action.MINUS) );

        this.findViewById(R.id.main_button_menu).setOnClickListener(
                new IRButtonListener(vibrator, this, IRButtonListener.Action.MENU) );

        this.findViewById(R.id.main_button_clock).setOnClickListener(
                new IRButtonListener(vibrator, this, IRButtonListener.Action.CLOCK) );

        this.findViewById(R.id.main_button_ok).setOnClickListener(
                new IRButtonListener(vibrator, this, IRButtonListener.Action.VENTILATION_OK) );

    }

    private void initOtherButtons() {
        this.findViewById(R.id.main_button_git_repo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://gitlab.com/snowtrackers/stove-italy-ir-remote"));
                startActivity(i);
            }
        });

        this.findViewById(R.id.main_button_credits).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isDisplayCredits = ! isDisplayCredits;
                findViewById(R.id.main_text_credits).setVisibility( isDisplayCredits ? View.VISIBLE : View.GONE );
            }
        });

        this.findViewById(R.id.main_button_licence).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isDisplayLicence = ! isDisplayLicence;
                findViewById(R.id.main_text_licence).setVisibility( isDisplayLicence ? View.VISIBLE : View.GONE );
            }
        });
    }

    public void displaySnackForNoIrEmitter() {
        Snackbar.make(this.findViewById(R.id.main_layout), R.string.main_snackbar_no_ir_emitter, Snackbar.LENGTH_LONG).show();
    }

    public IRController getIrController() {
        return this.irController;
    }
}