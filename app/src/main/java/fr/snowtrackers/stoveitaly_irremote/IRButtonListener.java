package fr.snowtrackers.stoveitaly_irremote;

import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;

import com.zoser.app.powermote.IRController;
import com.zoser.app.powermote.IRMessage;
import com.zoser.app.powermote.IRMessageRequest;
import com.zoser.app.powermote.IRNecFactory;

public class IRButtonListener implements View.OnClickListener {
   public enum Action {
      ON_OFF(0x0, 130), //0x41 //130
      PLUS(0x0, 2), //0x40 //2
      VENTILATION_OK(0x0, 136), //0x11 //136
      MINUS(0x0, 8), //0x10 //8
      MENU(0x0, 0x0),
      CLOCK(0x0, 128); //0x1 //128

      private final int address;
      private final int command;

      Action(int address, int command) {
         this.address = address;
         /*
         int myReversedCommand = 0;
         for (int i = 0; i < 8; i++) {
            myReversedCommand |= command & (int)Math.pow( 2, 7 - i );
         }
         this.command = myReversedCommand;
          */
         this.command = command;
      }

      public int getAddress() {
         return address;
      }

      public int getCommand() {
         return command;
      }

      public IRMessage createIRMessage() {
         return IRNecFactory.create( this.getCommand(), this.getAddress(), 1);
      }
   }

   private final Action action;

   private final Vibrator vibrator;

   private final MainActivity mainActivity;

   public IRButtonListener(Vibrator vibrator, MainActivity mainActivity, IRButtonListener.Action action) {
      this.action = action;
      this.vibrator = vibrator;
      this.mainActivity = mainActivity;
   }

   @Override
   public void onClick(View view) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
         this.vibrator.vibrate(VibrationEffect.createPredefined(VibrationEffect.EFFECT_CLICK));
      }
      else {
         this.vibrator.vibrate(60);
      }
      if( ! this.mainActivity.getIrController().hasIREmitter() ) {
         this.mainActivity.displaySnackForNoIrEmitter();
      }
      else {
         this.mainActivity.getIrController().sendMessage(new IRMessageRequest(this.action.createIRMessage()));
      }
   }
}
