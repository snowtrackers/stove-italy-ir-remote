# Stove Italy IR Remote

## Disclamer

This project is not linked to trademark and company Stove Italy. It's just a personal project.

My remote do not work well, so I've create a phone app to do the same.

[[_TOC_]]

## TODO

See issues

## Google Play Store

*Soon*

## Commands for Compact (and others ?)

### On/Off
    uint16_t command = 0x41;
    uint32_t data = 0xBE41FF00;

### Ventilation/OK
    uint16_t command = 0x11;
    uint32_t data = 0xEE11FF00;

### Clock
    uint16_t command = 0x1;
    uint32_t data = 0xFE01FF00;

### Menu
    uint16_t command = 0x0;
    uint32_t data = ;

### Command -
    uint16_t command = 0x10;
    uint32_t data = 0xEF10FF00;

### Command +
    uint16_t command = 0x40;
    uint32_t data = 0xBF40FF00;